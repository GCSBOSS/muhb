/* eslint-env mocha */
const assert = require('assert');
const http = require('http');
const crypto = require('crypto');
const muhb = require('../lib/main.js');
const { createReadStream } = require('fs');
const HTTPBIN_URL = process.env.HTTPBIN_URL || 'http://localhost:8066';

describe('Verbs', function(){

    it('Should properly reach a GET endpoint', async function(){
        const r = await muhb.get(HTTPBIN_URL + '/get');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        const o = JSON.parse(r.body);
        assert.strictEqual(typeof o.args, 'object');
    });

    it('Should properly reach a HEAD endpoint', async function(){
        const r = await muhb.head(HTTPBIN_URL + '/get');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        assert.strictEqual(r.body, '');
    });

    it('Should properly reach a POST endpoint', async function(){
        let r = await muhb.post(HTTPBIN_URL + '/post', 'balela');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        let o = JSON.parse(r.body);
        assert.strictEqual(o.data, 'balela');
        r = await muhb.post(HTTPBIN_URL + '/post', {'thak-thek': 'that'});
        o = JSON.parse(r.body);
        assert.strictEqual(o.headers['Thak-Thek'], 'that');
    });

    it('Should properly reach a PUT endpoint', async function(){
        let r = await muhb.put(HTTPBIN_URL + '/put', 'balela');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        let o = JSON.parse(r.body);
        assert.strictEqual(o.data, 'balela');
        r = await muhb.put(HTTPBIN_URL + '/put', {'thak-thek': 'that'});
        o = JSON.parse(r.body);
        assert.strictEqual(o.headers['Thak-Thek'], 'that');
    });

    it('Should properly reach a PATCH endpoint', async function(){
        let r = await muhb.patch(HTTPBIN_URL + '/patch', 'balela');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        let o = JSON.parse(r.body);
        assert.strictEqual(o.data, 'balela');
        r = await muhb.patch(HTTPBIN_URL + '/patch', {'thak-thek': 'that'});
        o = JSON.parse(r.body);
        assert.strictEqual(o.headers['Thak-Thek'], 'that');
    });

    it('Should properly reach a DELETE endpoint', async function(){
        const r = await muhb.delete(HTTPBIN_URL + '/delete');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        const o = JSON.parse(r.body);
        assert.strictEqual(typeof o.args, 'object');
    });

    it('Should properly reach an OPTIONS endpoint', async function(){
        const r = await muhb.options(HTTPBIN_URL + '/get');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers.allow, 'string');
        assert.strictEqual(r.body, '');
    });

    it('Should reject when request fails', async function(){
        await assert.rejects(muhb.request({
            method: 'GET',
            url: 'http://255.255.255.255:8347'
        }), /connect/);
    });

    it('Should send body from Buffer object', async function(){
        const { assert } = await muhb.post(
            HTTPBIN_URL + '/post',
            { 'Content-Type': 'application/json' },
            crypto.randomBytes(20)
        );
        assert.status.is(200);
        assert.body.contains('base64');
    });

});

describe('Automatic Headers', function(){

    it('Should generate automatic headers', async function(){
        const { body } = await muhb.post(HTTPBIN_URL + '/post');
        const o = JSON.parse(body);
        assert(o.headers.Date);
        assert(!o.headers['Content-Type']);
        assert.strictEqual(o.headers['Content-Length'], '0');
    });

    it('Should generate content length and type headers when body is present', async function(){
        const { body } = await muhb.post(HTTPBIN_URL + '/post', 'haha');
        const o = JSON.parse(body);
        assert(o.headers.Date);
        assert.strictEqual(o.headers['Content-Type'], 'text/plain');
        assert.strictEqual(o.headers['Content-Length'], '4');
    });

    it('Should prioritize user headers', async function(){
        const { body } = await muhb.post(HTTPBIN_URL + '/post', {'Content-Type': 'text/css'}, 'haha');
        const o = JSON.parse(body);
        assert.strictEqual(o.headers['Content-Type'], 'text/css');
    });

    it('Should not generate automatic headers when specified', async function(){
        const { body } = await muhb.post(HTTPBIN_URL + '/post', {'no-auto': true}, 'haha');
        const o = JSON.parse(body);
        assert(!o.headers.Date);
        assert(!o.headers['Content-Type']);
        assert(!o.headers['Content-Length']);
    });

});

describe('Root Definition', function(){

    it('Should properly reach a rooted GET endpoint', async function(){
        const base = muhb.context(HTTPBIN_URL);
        assert('post' in base);
        assert('get' in base);
        const r = await base.get('/get');
        assert.strictEqual(r.status, 200);
        assert.strictEqual(typeof r.headers, 'object');
        const o = JSON.parse(r.body);
        assert.strictEqual(typeof o.args, 'object');
    });

});

describe('Assertions', function(){

    var ep = muhb.context(HTTPBIN_URL);

    it('Should be ok asserting truths over http status code', async function(){
        const { assert: ass } = await ep.get('/get');
        assert.doesNotThrow( () => ass.status.is(200) );
        assert.doesNotThrow( () => ass.status.in([200, 201])  );
        assert.doesNotThrow( () => ass.status.not(201) );
        assert.doesNotThrow( () => ass.status.notIn([202, 201]) );
        assert.doesNotThrow( () => ass.status.type(2) );
        assert.doesNotThrow( () => ass.status.notType(3) );
    });

    it('Should throw asserting falsehoods over http status code', async function(){
        const { assert: ass } = await ep.get('/get');
        assert.throws( () => ass.status.is(201) );
        assert.throws( () => ass.status.in([202, 201])  );
        assert.throws( () => ass.status.not(200) );
        assert.throws( () => ass.status.notIn([200, 201]) );
        assert.throws( () => ass.status.type(3) );
        assert.throws( () => ass.status.notType(2) );
    });

    it('Should be ok asserting truths over http headers', async function(){
        const { assert: ass } = await ep.get('/get');
        assert.doesNotThrow( () => ass.headers.has('content-length').has('date') );
        assert.doesNotThrow( () => ass.headers.match('content-type', 'application/json') );
    });

    it('Should throw asserting falsehoods over http headers', async function(){
        const { assert: ass } = await ep.get('/get');
        assert.throws( () => ass.headers.has('x-none') );
        assert.throws( () => ass.headers.match('content-type', 'text/html') );
    });

    it('Should be ok asserting truths over response body', async function(){
        const { assert: ass } = await ep.get('/user-agent');
        assert.doesNotThrow( () => ass.body.exactly('{\n  "user-agent": null\n}\n') );
        assert.doesNotThrow( () => ass.body.contains('user-agent') );
        assert.doesNotThrow( () => ass.body.match(/null/g) );
        assert.doesNotThrow( () => ass.body.type('application/json') );
        assert.doesNotThrow( () => ass.body.length(25) );
    });

    it('Should throw asserting falsehoods over response body', async function(){
        const { assert: ass } = await ep.get('/');
        assert.throws( () => ass.body.exactly('barfoo') );
        assert.throws( () => ass.body.contains('baz') );
        assert.throws( () => ass.body.match(/bah/) );
        assert.throws( () => ass.body.type('image/jpg') );
        assert.throws( () => ass.body.length(1) );
        assert.throws( () => ass.body.json );
    });

    it('Should be ok asserting truths over json response body', async function(){
        const { assert: ass } = await ep.get('/user-agent');
        const assJSON = ass.body.json;
        assert.doesNotThrow( () => assJSON.hasKey('user-agent') );
        assert.doesNotThrow( () => assJSON.match('user-agent', null) );
        delete assJSON.object['user-agent'];
        assert.doesNotThrow( () => assJSON.empty() );
    });

    it('Should throw asserting falsehoods over json response body', async function(){
        const { assert: ass } = await ep.get('/user-agent');
        const assJSON = ass.body.json;
        assert.throws( () => assJSON.hasKey('barfoo') );
        assert.throws( () => assJSON.match('user-agent', 'bah') );
        assert.throws( () => assJSON.empty(/bah/) );
        assert.throws( () => assJSON.array );
    });

    it('Should be ok asserting truths over json array response body', async function(){
        const { assert: ass } = await ep.get('/user-agent');
        const assJSON = ass.body.json;
        assJSON.object = [ 'foo', 'bar' ];
        const assArr = assJSON.array;
        assert.doesNotThrow( () => assArr.match(1, 'bar') );
        assert.doesNotThrow( () => assArr.includes('bar') );
        assert.doesNotThrow( () => assArr.length(2) );
        assJSON.object = [];
        assert.doesNotThrow( () => assJSON.array.empty() );
    });

    it('Should throw asserting falsehoods over json array response body', async function(){
        const { assert: ass } = await ep.get('/user-agent');
        const assJSON = ass.body.json;
        assJSON.object = [ 'bar', 'foo' ];
        const assArr = assJSON.array;
        assert.throws( () => assArr.match(1, 'bar') );
        assert.throws( () => assArr.includes('baz') );
        assert.throws( () => assArr.length(1) );
        assert.throws( () => assArr.empty() );
    });

});

describe('Timeout', function(){

    it('Should reject when response time exceeds setup timeout', async function(){
        await assert.rejects(() => muhb.request({
            method: 'GET',
            url: HTTPBIN_URL + '/delay/10000',
            timeout: 500
        }), /timeout/g);

        await assert.rejects(() => muhb.request({
            method: 'GET',
            url: HTTPBIN_URL + '/delay/10000',
            headers: { timeout: 500 }
        }), /timeout/g);
    });

    it('Should timeout well when there is authentication', async function(){
        const server = http.createServer((req, res) => {
            setTimeout(() => res.end(), 10000);
        });
        server.listen(8455);
        await assert.rejects(muhb.get('http://john:doe@localhost:8455/foobar', { timeout: 1400 }), /1400ms/);
        server.close();
    });

    it('Should not timeout when authentication fails', async function(){

        const server = http.createServer((req, res) => {
            res.statusCode = 401;
            res.setHeader('www-authenticate', 'Digest realm="http-auth@example.org", ' +
                'qop="auth", algorithm=MD5, ' +
                'nonce="7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v", ' +
                'opaque="FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"');

            res.end();
        });

        server.listen(8455);
        const { status } = await muhb.get('http://john:doe@localhost:8455/foobar', { timeout: 1800 });
        assert.strictEqual(status, 401);
        server.close();
    });

    it('Should not timeout when authentication succeeds', async function(){

        const server = http.createServer((req, res) => {
            if(!req.headers.authorization){
                res.statusCode = 401;
                res.setHeader('www-authenticate', 'Digest realm="http-auth@example.org", ' +
                    'qop="auth", algorithm=MD5, ' +
                    'nonce="7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v", ' +
                    'opaque="FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"');

                return res.end();
            }
            res.write('OK');
            res.end();
        });

        server.listen(8455);
        const { status } = await muhb.get('http://john:doe@localhost:8455/foobar', { timeout: 1800 });
        assert.strictEqual(status, 200);
        server.close();
    });


});

describe('Pooling', function(){

    it('Should reach the endpoint the correct amount of times', function(done){
        this.timeout(5000);
        const p = new muhb.Pool();
        'abcd'.split('').forEach(b => p.post(HTTPBIN_URL + '/delay/2', b));

        p.on('finish', function(resArr){
            assert.strictEqual(resArr.length, 4);
            resArr.forEach( res => res.assert.status.is(200) );
            done();
        });
    });

    it('Should timeout requests according to pool rules', async function(){
        this.timeout(5000);

        let errs = 0;
        const p = new muhb.Pool({ size: 2, timeout: 2300 });
        '2232'.split('').forEach(
            d => p.post(HTTPBIN_URL + '/delay/' + d).catch(() => errs++ ));

        const resArr = await p.done();
        assert.strictEqual(resArr.length, 4);
        resArr[1].assert.status.is(200);
        assert(resArr[3].message.indexOf('timeout 2300ms') > -1);
        assert.strictEqual(errs, 1);
    });

    it('Should allow updating pool settings on the fly', async function(){
        this.timeout(4500);
        const p = new muhb.Pool({ size: 1 });
        await p.post(HTTPBIN_URL + '/delay/2', { 'X-Header': 'Coverage' });
        p.size = 3;
        p.post(HTTPBIN_URL + '/delay/2');
        p.post(HTTPBIN_URL + '/delay/2');
        p.post(HTTPBIN_URL + '/delay/2');
        await p.done();
    });

    it('Should stop processing queued requests when paused', async function(){
        const p = new muhb.Pool({ size: 2 });
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        await p.pause();
        assert.strictEqual(p.queue.length, 3);
    });

    it('Should finish processing when resuming a paused pool', async function(){
        this.timeout(3500);
        const p = new muhb.Pool({ size: 2 });
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        p.post(HTTPBIN_URL + '/delay/1');
        await p.pause();
        const r = await p.resume();
        assert.strictEqual(r.length, 3);
        assert.strictEqual(p.queue.length, 0);
    });

});

describe('HTTPS', function(){

    it('Should send HTTPS requests just fine', async function(){
        const r = await muhb.get('https://example.com');
        r.assert.status.is(200);
        r.assert.body.contains('Example Domain');
    });

});

describe('Cookies', function(){

    it('Should parse cookies set by the server', async function(){
        const { cookies } = await muhb.get(HTTPBIN_URL + '/cookies/set/test/foobar');
        assert.strictEqual(cookies.test, 'foobar');
    });

    it('Should send cookies given on input object', async function(){
        const { cookies } = await muhb.get(HTTPBIN_URL + '/cookies/set/test/foofoo');
        const { assert } = await muhb.get(HTTPBIN_URL + '/cookies', { cookies });
        assert.body.contains('foofoo');
    });

});

describe('Auth', function(){

    it('Should pass basic auth by default', async function(){
        const { assert } = await muhb.get(
            HTTPBIN_URL + '/basic-auth/my-user/my-pwd',
            { auth: { username: 'my-user', password: 'my-pwd' } }
        );
        assert.status.is(200);
        assert.body.json.match('authenticated', true);
    });

    it('Should respond to MD5 Digest challenge', async function(){
        const { assert } = await muhb.get(
            HTTPBIN_URL + '/digest-auth/auth/my-user/my-pwd/MD5',
            { auth: { username: 'my-user', password: 'my-pwd' } }
        );
        assert.status.is(200);
        assert.body.json.match('authenticated', true);
    });

    it('Should accept user and password via URL', async function(){
        const url = HTTPBIN_URL.split('//').join('//usr:pwd@');
        const { assert } = await muhb.get(url + '/digest-auth/auth/usr/pwd/MD5');
        assert.status.is(200);
        assert.body.json.match('authenticated', true);
    });

    it('Should repeat request in case probe request is not challenged', async function(){
        const server = http.createServer((req, res) => {
            res.statusCode = 404;
            req.on('data', buf => res.write(buf));
            setTimeout(() => res.end(), 200);
        });

        server.listen(8455);
        const { status, body } = await muhb.post('http://john:doe@localhost:8455/foobar', {}, 'foo');
        assert.strictEqual(status, 404);
        assert.strictEqual(body.toString(), 'foo');
        server.close();
    });

});

describe('Regressions', function(){

    it('Should send all http methods through the pool', async function(){
        const p = new muhb.Pool();
        const r1 = await p.post(HTTPBIN_URL + '/post');
        const r2 = await p.put(HTTPBIN_URL + '/put');
        assert.strictEqual(r1.status, 200);
        assert.strictEqual(r2.status, 200);
    });

    it('Should properly authenticate even when streaming the request', async function(){

        const server = http.createServer((req, res) => {
            if(!req.headers.authorization){
                res.statusCode = 401;
                res.setHeader('www-authenticate', 'Digest realm="http-auth@example.org", ' +
                    'qop="auth", algorithm=MD5, ' +
                    'nonce="7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v", ' +
                    'opaque="FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"');

                return res.end();
            }

            req.on('data', buf => {
                assert.strictEqual(buf.toString('utf8'), 'abcdef');
                res.flushHeaders();
                setTimeout(() => {
                    res.write('OK');
                    res.end();
                }, 500);
            });
        });

        server.listen(8455);

        const req = muhb.post('http://john:doe@localhost:8455/foobar', { stream: true });
        setTimeout(() => req.write('abcdef'), 500);

        const res = await req;

        await new Promise(done => res.on('data', buf => {
            assert.strictEqual(buf.toString('utf8'), 'OK');
            setTimeout(() => {
                req.end();
                server.close();
                done();
            }, 500);
        }));
    });

});

describe('Other Features', function(){

    it('Should return buffers for binary response bodies', async function(){
        const { body } = await muhb.get(HTTPBIN_URL + '/bytes/16');
        assert(body instanceof Buffer);
        assert.strictEqual(body.length, 16);
    });

    it('Should keep request open when setup', function(done){
        const s = http.createServer(function(req, res){
            res.flushHeaders();
            req.on('data', d => {
                assert.strictEqual(d.toString(), 'aaa');
                res.end();
                done();
            });
        });
        s.listen(12345);

        muhb.post('http://localhost:12345/', { stream: true }).then(res => {
            res.req.write('aaa');
            s.close();
        });

    });

    it('Should accept streams as request body', async function(){
        // process.addListener('unhandledRejection', (err) => console.log(err));
        const FormData = require('form-data');

        const server = http.createServer((req, res) => {
            if(!req.headers.authorization){
                res.statusCode = 401;
                res.setHeader('www-authenticate', 'Digest realm="http-auth@example.org", ' +
                    'qop="auth", algorithm=MD5, ' +
                    'nonce="7ypf/xlj9XXwfDPEoM4URrv/xwf94BcCAzFZH4GiTo0v", ' +
                    'opaque="FQhe/qaU925kfnzjCev0ciny7QMkPqMAFRtzCUYo5tdS"');

                return res.end();
            }

            req.once('data', buf => {
                assert(buf.length > 0);
                res.flushHeaders();
                setTimeout(() => {
                    res.write('OK');
                    res.end();
                }, 500);
            });
        });

        server.listen(8455);

        const fd = new FormData();
        fd.append('file', createReadStream(__filename));

        const res = await muhb.post('http://john:doe@localhost:8455/foobar', {
            ...fd.getHeaders()
        }, fd);

        assert.strictEqual(res.status, 200);
        assert.strictEqual(res.body.toString(), 'OK');
        server.close();
    });

});
