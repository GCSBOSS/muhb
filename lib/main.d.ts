import { ServerResponse, ClientRequest } from 'http';

declare namespace MUHB {

    /**
     * This is basically a Node `ServerResponse` containing some extra properties.
     */
    class Response extends ServerResponse {
        /** Response status code */
        status: number
        /** Response body parsed acordingly to response headers. Undefined in case of stream request */
        body?: unknown
        /** Object containing the cookies parsed from the response headers */
        cookies: Record<string, string>
    }

    /**
     * This is basically a Node `ClientRequest` object that can be treated as a promise.
     */
    type Request = Promise<Response> & ClientRequest;

    type RequestSettings = {
        /** Socket timeout for the request in milliseconds */
        timeout?: number,
        /** When set to true, does not read the response body */
        stream?: boolean,
        /** Object containing the cookies to be sent along with the request headers */
        cookies?: Record<string, string>,
        /** Authentication credentials to be sent in case the server requires them */
        auth?: { username: string, password?: string }
    }

    type Settings = {
        /** Request HTTP Method */
        method?: string,
        /** Request URL */
        url: string,
        /** Object containing headers to be sent in the request  */
        headers?: Record<string, string>,
        /** Any value to be sent as the request body. Request content headers will be set accordingly unless they are explicitly set in the request. */
        body?: unknown
    } & RequestSettings;

    type Headers = Record<string, string> & RequestSettings;
}

/**
 * @param string url The request URL
 * @param headers An object containing the headers to be sent along with the request
 *     You can also pass request settings in this object.
 * @param body - Any value to be sent as the request body.
 *      Content headers will be set accordingly unless they are explicitly set in the request.
 */
 function cm(url: string, headers?: MUHB.Headers, body?: unknown): MUHB.Request

 /**
  * @param url The request URL
  * @param headers An object containing the headers to be sent along with the request.
  *     You can also pass request settings in this object.
  */
 function cm(url: string, headers?: MUHB.Headers): MUHB.Request

 /**
  * @param url The request URL
  * @param body - Any value to be sent as the request body.
  *      Content headers will be set accordingly unless they are explicitly set in the request.
  */
 function cm(url: string, body?: unknown): MUHB.Request


/**
 * Performs an HTTP request
 *
 * Example usage:
 * ```js
 * const { status, headers, body } = await muhb('post', 'http://example.com',
 *     { 'x-opt-header': 'foobar' }, { foo: 'will send a JSON body' });
 * ```
 */
declare const muhb: {

    /**
     * @param method The request HTTP method
     * @param url The request URL
     * @param headers An object containing the headers to be sent along with the request
     * @param body - Any value to be sent as the request body.
     *      Content headers will be set accordingly unless they are explicitly set in the request.
     */
    (method: string, url: string, headers?: MUHB.Headers, body?: unknown): MUHB.Request,

    /**
     * @param method The request HTTP method
     * @param url The request URL
     * @param headers An object containing the headers to be sent along with the request
     */
    (method: string, url: string, headers?: MUHB.Headers): MUHB.Request,

    /**
     * @param method The request HTTP method
     * @param url The request URL
     * @param body - Any value to be sent as the request body.
     *      Content headers will be set accordingly unless they are explicitly set in the request.
     */
    (method: string, url: string, body?: unknown): MUHB.Request,

    /**
     * Performs a POST request
     *
     * Example usage:
     * ```js
     * const { status, body } = await post('http://example.com', 'body data');
     * ```
     */
    post: typeof cm,

    /**
     * Performs a PUT request
     *
     * Example usage:
     * ```js
     * const { status, body } = await put('http://example.com', 'body data');
     * ```
     */
    put: typeof cm,

    /**
     * Performs a GET request
     *
     * Example usage:
     * ```js
     * const { status, body } = await get('http://example.com');
     * ```
     */
    get: typeof cm,

    /**
     * Performs a DELETE request
     *
     * Example usage:
     * ```js
     * const { status, body } = await del('http://example.com');
     * ```
     */
    del: typeof cm,

    /**
     * Performs a PATCH request
     *
     * Example usage:
     * ```js
     * const { status, body } = await patch('http://example.com', 'body data');
     * ```
     */
    patch: typeof cm,

    /**
     * Performs a HEAD request
     *
     * Example usage:
     * ```js
     * const { status, body } = await head('http://example.com');
     * ```
     */
    head: typeof cm,

    /**
     * Performs an OPTIONS request
     *
     * Example usage:
     * ```js
     * const { status, body } = await options('http://example.com');
     * ```
     */
    options: typeof cm

    /**
     * Performs an HTTP request
     *
     * Example usage:
     * ```js
     * const { status, body } = await request({
     *     url: 'http://example.com',
     *     method: 'POST',
     *     body: 'body data',
     *     headers: { 'x-my-header': 123 }
     *     // other settings...
     * });
     * ```
     */
    request(settings: MUHB.Settings): MUHB.Request
}

export = muhb
