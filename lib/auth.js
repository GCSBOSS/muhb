const crypto = require('crypto');

let NC = 0;

const md5 = input =>  crypto.createHash('md5').update(input).digest('hex');

const methods = {

    digest(auth, opts, data) {
        const cnonce = crypto.randomBytes(8).toString('hex');
        const nc = String(++NC).padStart(8, '0');

        // TODO add support to other algorithms
        const ha1 = md5(`${auth.username}:${data.realm}:${auth.password}`);
        //if(data.algorithm && data.algorithm.toLowerCase() === 'md5-sess')
        //    ha1 = md5(ha1 + ':' + data.nonce + ':');

        const ha2 = md5(opts.method.toUpperCase() + ':' + opts.path);

        const [qop] = data.qop.split(/[, ]+/);

        // TODO add support for no QOP (httpbin won't support testing it).
        const response = md5(`${ha1}:${data.nonce}:${nc}:${cnonce}:${qop}:${ha2}`);
        //qop ? ... : md5(`${ha1}:${data.nonce}:${ha2}`);

        return {
            username: auth.username, response, realm: data.realm, nc,
            nonce: data.nonce, cnonce, uri: opts.path, qop,
            opaque: data.opaque, algorithm: data.algorithm
        };
    },

    basic(auth){
        return Buffer.from(`${auth.username}:${auth.password}`).toString('base64');
    }

};

function parseFields(header){
    const data = {}
    const regex = /([a-z0-9_-]+)=(?:"([^"]+)"|([a-z0-9_-]+))/gi;
    while(true){
        const m = regex.exec(header);
        if(!m)
            break;
        data[m[1]] = m[2] || m[3];
    }
    return data;
}

module.exports = function build(auth, opts, input){


    const [ type ] = input.split(' ');

    const inData = parseFields(input);
    const fn = methods[type.toLowerCase()] || methods.basic;
    let outData = fn(auth, opts, inData);

    if(typeof outData == 'object'){
        const props = [];
        for(const k in outData)
            outData[k] && props.push(`${k}="${outData[k]}"`);
        outData = props.join(', ');
    }

    return type + ' ' + outData;
};
