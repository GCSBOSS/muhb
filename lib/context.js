const assert = require('assert');

module.exports = function getContextFunction(verbs, muhb){

    return function context(url){
        process.emitWarning('context() will be removed on v4.0.0', {
            type: 'DeprecatioWarning'
        });
        assert.strictEqual(typeof url, 'string');
        url = url.replace(/\/?$/g, '/');

        const ctx = {};
        for(const m of verbs)
            ctx[m] = (u, h, b) => muhb(m.toUpperCase(), url + u, h, b);
        ctx.del = ctx.delete;
        return ctx;
    }

}
