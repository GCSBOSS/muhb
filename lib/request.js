const http = require('http');
const https = require('https');
const getAssertions = require('./assertions');
const buildAuth = require('./auth');
const { Stream } = require('stream');

function parseCookies(cookies){
    const r = {};

    if(Array.isArray(cookies))
        for(const c of cookies){
            const [ key, value ] = c.split(';')[0].split('=');
            r[key] = decodeURIComponent(value);
        }

    return r;
}

function stringifyCookies(cookies){
    let r = '';
    for(const name in cookies)
        r += name + '=' + encodeURIComponent(cookies[name]) + '; ';
    return r;
}

function prepare(input){
    const url = new URL(input.url);

    input.headers = typeof input.headers == 'object' ? input.headers : {};

    const auth = input.headers.auth || {};
    const settings = {
        stream: input.headers.stream || input.stream,
        autoHeaders: !input.headers['no-auto'] || false,
        cookies: input.cookies || input.headers.cookies || false,
        timeout: input.timeout || input.headers.timeout || false,

        auth: auth.username || url.username ? {
            username: auth.username || url.username,
            password: auth.password || url.password
        } : false
    };


    const headers = { ...input.headers };

    if(settings.cookies)
        headers.Cookie = stringifyCookies(settings.cookies);

    if(settings.auth)
        headers.Connection = 'keep-alive';

    let payload, type, bodyStream;
    if(input.body instanceof Buffer)
        payload = input.body;
    else if(input.body instanceof Stream)
        bodyStream = input.body;
    else if(typeof input.body == 'object'){
        type = 'application/json';
        payload = Buffer.from(JSON.stringify(input.body));
    }
    else if(input.body){
        type = 'text/plain';
        payload = Buffer.from(String(input.body));
    }

    if(settings.autoHeaders){

        if(payload?.length)
            headers['Content-Length'] = payload.length;

        headers.Date = new Date().toUTCString();

        if(type && !headers['Content-Type'])
            headers['Content-Type'] = type;
    }

    delete headers.auth;
    delete headers.cookies;
    delete headers['no-auto'];
    delete headers.stream;

    const client = url.protocol === 'https:' ? https : http;

    const options = {
        headers,
        method: input.method,
        hostname: url.hostname,
        port: url.port,
        path: url.pathname + url.search
    };

    if(settings.timeout)
        options.timeout = settings.timeout;

    return { input, options, settings, client, payload, bodyStream };
}

function parseResponseBody(res){

    return new Promise(done => {

        // TODO should rely on charset portion of content-type; breaking change.
        const ct = res.headers['content-type'];
        if(/(^text\/|\/json$)/.test(ct)){

            // TODO should parse JSON types; breaking change

            res.body = '';
            res.setEncoding('utf8');
            res.on('data', chunk => res.body += chunk);
            res.on('end', () => done());
            return;
        }


        res.body = [];
        res.on('data', chunk => res.body.push(chunk));
        res.on('end', () => {
            res.body = Buffer.concat(res.body)
            done();
        });
    });

}

function writeRequest(ctx, req){

    if(ctx.settings.stream && req.final)
        return req.flushHeaders();

    if(req.final && ctx.payload)
        req.write(ctx.payload);

    if(req.final && ctx.bodyStream)
        ctx.bodyStream.pipe(req);
    else
        req.end();
}

function handleRequest(ctx, req, resolve, reject){
    !ctx.settings.stream && req.on('error', reject);
    req.on('timeout', () =>
        req.destroy(new Error('Request timeout ' + ctx.settings.timeout + 'ms')));

    req.on('response', async res => {
        res.on('error', reject);

        if(req.noCred && !ctx.finalReq.finished){

            if(res.statusCode == 401 && res.headers['www-authenticate'])
                ctx.finalReq.setHeader('authorization', buildAuth(
                    ctx.settings.auth,
                    ctx.options,
                    res.headers['www-authenticate']
                ));

            writeRequest(ctx, ctx.finalReq);
        }

        else if(req.final){
            res.status = res.statusCode;
            res.assert = getAssertions(res);
            res.cookies = parseCookies(res.headers['set-cookie']);
            (!ctx.settings.stream || res.status > 299) && await parseResponseBody(res);
            resolve?.(res);
        }
    });

    if(req.final && ctx.settings.auth)
        return;

    writeRequest(ctx, req);

}

module.exports = {

    request(input){
        const ctx = prepare(input);

        const finalReq = ctx.client.request(ctx.options);
        finalReq.final = true;
        ctx.finalReq = finalReq;

        const promise = new Promise((resolve, reject) => {

            if(ctx.settings.auth){
                const headers = { ...ctx.options.headers };
                const challengeReq = ctx.client.request({ ...ctx.options, headers });
                challengeReq.removeHeader('content-length');
                challengeReq.removeHeader('content-type');
                challengeReq.removeHeader('content-encoding');
                challengeReq.noCred = true;
                handleRequest(ctx, challengeReq, resolve, reject);
            }

            handleRequest(ctx, finalReq, resolve, reject);
        });

        finalReq.then = promise.then.bind(promise);
        finalReq.catch = promise.catch.bind(promise);

        return finalReq;
    },

    METHODS: [
        'get',
        'put',
        'head',
        'post',
        'trace',
        'patch',
        'delete',
        'connect',
        'options'
    ]

}
